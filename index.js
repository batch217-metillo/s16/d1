console.log("Hello World!");


// [SECTION] Arithmetic Operators
// + , - , * , /

let	x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);


// [SECTION] Assignment Operator
// Basic Assignment operator is (=) equal sign.

let	assignmentNumber = 8;

// Addition Assignment
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator:" + assignmentNumber);

// Shorthand for assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log("Result of addition assignment operator:" + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator:" + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator:" + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator:" + assignmentNumber);

assignmentNumber %= 2;
console.log("Result of modulo assignment operator:" + assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition assignment operator:" + assignmentNumber);


// [SECTION] Multiple Operators and Parenthesis

let	mdas = 1 + 2 - 3 * 4 / 5
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);


// [SECTION] Incrementation and Decrementation
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

// Pre-incrementation
let increment = ++z 	// PLUS 1 AGAD
console.log("Result of pre-increment: " + increment)
console.log("Result of pre-increment: " + z)

// Post-incrementation
increment = z++;		// SAME PA RIN MUNA SA NA-UNANG INCREMENT
increment = z++;		// SAKA LANG ULIT MAG-E-INCREMENT
console.log("Result of post-increment: " + increment)

// Decrementation
// pre-decrementation
let decrement = --z;	// SAME PA RIN MUNA SA NA-UNANG INCREMENT
// let decrement = --z;	// SAKA LANG MAGDE-DECREMENT
console.log("Result of pre-decrement: " + decrement)
console.log("Result of pre-decrement: " + z)

// post-decrementation
decrement = z--;
console.log("Result of post-decrement: " + decrement)
console.log("Result of post-decrement: " + z)


// [SECTION] Type Coercion
// Type coercion is the automatic or implicit conversion of values from one data type to another

let numA = '10';
let numB = 12;

let coercion  = numA + numB;
console.log(coercion); //same lang bale may ko-concatenate lang result = 1012
console.log(typeof coercion);

// let numA1 = 10;
// let numB1 = '12';

// let coercion  = numA1 + numB1;
// console.log(coercion);
// console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD
console.log(nonCoercion);
console.log(typeof nonCoercion);

// TRUE = 1
let numE = true + 1;
console.log(numE);

// FALSE = 0
let numF = false + 1;
console.log(numF);


// [SECTION] Comparison Operators
let juan = "juan";

// [SECTION] Equality Operator (==)
// = assignment of value
// == comparison equality

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1'); //This is true dahil regardless of  the data type
console.log(0 == false);
console.log("juan" == "juan");
console.log("juan" == juan);

// [SECTION] Inequality Operator (!=)
// ! means not

console.log(1 != 1);
console.log(1 != 2);

// [SECTION] Strict Equality Operator (===)
/*
	-Checks if the value or operand are equal are of the same type
*/

console.log(1 === '1'); //This is false since they are not on the same type
console.log("juan" === juan);
console.log(0 === false);

// [SECTION] Strict Inequality Operator (!==)

console.log(1 !== '1');
console.log("juan" !== juan);
console.log(0 !== false);

// [SECTION] Relational Operator
// Some comparison operators check whether one value is greater or less than to the other value.
// >, <, =, <=, >=

let a = 50;
let b = 65;

let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);

let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

// [SECTION] Logical Operator
// && --> AND, || --> OR, ! --> NOT

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered
console.log(" Logical && Result: " + allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered
console.log(" Logical || Result: " + someRequirementsMet);

let someRequirementsNotMet = !isRegistered
console.log(" Logical ! Result: " + someRequirementsNotMet);